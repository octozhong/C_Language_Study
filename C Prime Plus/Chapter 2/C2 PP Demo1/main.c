#include <stdio.h>
#include <stdlib.h>

//本题目考量的是printf和\n的用法
int main(void)
{
    printf("Black Clover\n");
    printf("Black\nClover\n");
    printf("Black ");
    printf("Clover\n");

    return 0;
}

