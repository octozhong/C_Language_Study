#include <stdio.h>
#include <stdlib.h>

//一个文件中包含两个函数

void butler(void);

int main()
{
    printf("I will summon the butler fuction.\n");
    butler();
    printf("Yes. Bring me some tea and writeable DVDs.\n");
    return 0;
}

void butler(void){
    printf("You rang, sir?\n");
}
