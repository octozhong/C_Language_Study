# 程序设计
## 实现目标
当n=5时候，n的2次方和n的3次方各为多少
## version1.0
> 代码

        #include <stdio.h>

        int main(void){
            int n,int n2,int n3;
            /*本程序有多处错误，请勘误
            n=5;
            n2=n*n;
            n3=n2*n2;
            printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3)
            return 0;
        }

> 程序报错

        ||=== Build: Debug 在 C2 EG Demo2.7 中 (编译器: GNU GCC Compiler) ===|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c||In function 'main':|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|5|error: expected identifier or '(' before 'int'|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|8|error: 'n2' undeclared (first use in this function); did you mean 'n'?|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|8|note: each undeclared identifier is reported only once for each function it appears in|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|9|error: 'n3' undeclared (first use in this function); did you mean 'n'?|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|10|error: expected ';' before 'return'|
        ||=== 构建 失败: 4 error(s), 0 warning(s) (0 分, 0 秒) ===|

> 问题分析

        从报错来分析的话是在1.0版本中，
        一、注释并未闭合，解决方案有两种：
                1，将“/*本程序有多处错误，请勘误”的“勘误”后面添加“*/”。
                2，将“/*本程序有多处错误，请勘误”变为“//本程序有多处错误，请勘误”。
        二、在“int n,int n2,int n3;”中并不能这么写，可修改改为如下两种：
                1，int n,n2,n3;
                2，int n;
                   int n2;
                   int n3;
        

## version2.0
>代码

        #include <stdio.h>

        int main(void){
            int n;
            int n2;
            int n3;
            /*本程序有多处错误，请勘误*/
            n=5;
            n2=n*n;
            n3=n2*n2;
            printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3)
            return 0;
        }

>程序报错

        ||=== Build: Debug 在 C2 EG Demo2.7 中 (编译器: GNU GCC Compiler) ===|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c||In function 'main':|
        D:\Programming\C\C Prime Plus\C2 EG DEMO2.7\main.c|12|error: expected ';' before 'return'|
        ||=== 构建 失败: 1 error(s), 0 warning(s) (0 分, 0 秒) ===|

> 问题分析

        一、在“ printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3)”并未用“;”结尾，解决方案：
                在： printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3)后添加“;”即可。

## version3.0
>代码

        #include <stdio.h>

        int main(void){
            int n;
            int n2;
            int n3;
            /*本程序有多处错误，请勘误*/
            n=5;
            n2=n*n;
            n3=n2*n2;
            printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3);
            return 0;
        }

>程序运行结果

        n = 5,n squared = 25,n cube = 625
        Process returned 0 (0x0)   execution time : 0.026 s
        Press any key to continue.

> 问题分析

        一、本程序已经可以正常运行了。
        二、在计算结果上还有些许错误，本程序的目的是计算并打印出在“n=5”的情况下，“n,n^2,n^3”的值分别为多少，但是在计算中，原本应为 “ n = 5,n squared = 25,n cube = 125 ”，错误应该为代码中的“n3”,若以代码中来算，“n3”计算的为n的四次方而不是三次方。

## LastVersion

>代码

        #include <stdio.h>

        int main(void){
            int n;
            int n2;
            int n3;
            /*本程序有多处错误，请勘误*/
            n=5;
            n2=n*n;
            n3=n2*n;
            printf("n = %d,n squared = %d,n cube = %d\n",n,n2,n3);
            return 0;
        }

>程序运行结果

        n = 5,n squared = 25,n cube = 125
        Process returned 0 (0x0)   execution time : 0.026 s
        Press any key to continue.
