#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("starting now:\n");
    one_three();
    printf("done!");
    return 0;
}

int one_three(void){
    printf("one\n");
    two();
    printf("three\n");
}

int two(void){
    printf("two\n");
}
