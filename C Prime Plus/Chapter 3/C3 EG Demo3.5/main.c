#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;

    printf("Please enter a character.\n");
    //scanf("%c",&ch);
    ch=getchar();
    printf("putchar=");
    putchar(ch);
    printf("\nThe code for %c is %d .\n", ch, ch);
    return 0;
}
