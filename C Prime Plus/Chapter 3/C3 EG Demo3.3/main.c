#include <stdio.h>
#include <stdlib.h>

int main()
{
    int x =100;

    printf("dec = %d ; octal = %o ; hex =%x\n",x,x,x);
    //输出 dec = 100 ; octal = 144 ; hex =64
    printf("dec = %#d ; octal = %#o ; hex =%#x\n",x,x,x);
    //输出 dec = 100 ; octal = 0144 ; hex =0x64
    return 0;
}
