#include <stdio.h>
#include <stdlib.h>

int main()
{
    float weight;
    float value;

    printf("Are you worth your weight in platinum?\n");
    printf("Let's check it out.\n");
    printf("Please enter your weight in pounds:");

    scanf("%f",&weight);
    value = 1700.0 * weight * 14.5833;
    printf("Your weight in platinum is worth $%.2f.\n",value);
    //$%.2f.\n中，“$”为输出符号，“%.2f”意味着输出保留两位小数，“.”为结束，“\n”为换行

    printf("You are easily worth that ! If platinum prices drop,\n");
    printf("eat more to maintain your value.\n");

    return 0;
}
