#include <stdio.h>
#include <stdlib.h>

int main()
{
    int ten = 10;
    int two =2;
    printf("Doing it right:\n");
    printf("%d minus %d is %d\n",ten , 2 ,ten-two);
    printf("Doing it wrong:\n");
    printf("%d minus %d is %d\n",ten);
    //由于本代码中只有一个%d赋值了参数为ten，因此其他参数为内存中的任意值
    return 0;
}
