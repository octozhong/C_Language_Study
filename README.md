# C-Language-Study

#### 介绍

本项目参考两本书：《C Prime Plus（第六版）》和《C程序设计语言（第二版）》

#### IDE

名称 : Code::Blocks

版本 : 20.03-r11983

SDK Version: 2.0.0

Scintilla Version: 3.7.5

#### 编译环境

1. gcc (x86_64-posix-seh-rev0, Built by MinGW-W64 project) 8.1.0
2. g++ (x86_64-posix-seh-rev0, Built by MinGW-W64 project) 8.1.0

#### 参考文献

[1]	Prata.S  C Primer Plus , Sixth Edition  [M] 北京:人民邮电出版社 , 2019;

[2]	Kernighan, B. W.  and Ritchie D. M. The C Programming Language , Second Edition [M] 北京:机械工业出版社 , 2021;
